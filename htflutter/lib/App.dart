import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:htflutter/blocs/cart/cart_bloc.dart';
import 'package:htflutter/blocs/products/products_bloc.dart';
import 'package:htflutter/common/theme/theme.dart';
import 'package:htflutter/screens/CartScreen.dart';
import 'package:htflutter/screens/HomeScreen.dart';
import 'package:htflutter/screens/NewProductScreen.dart';
import 'package:htflutter/screens/ProductDetailsScreen.dart';

class App extends StatelessWidget {
  const App({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (context) => CartBloc(),
        ),
        BlocProvider(
          create: (context) => ProductsBloc(),
        ),
      ],
      child: MaterialApp(
        title: 'Flutter Demo',
        theme: lightTheme,
        darkTheme: darkTheme,
        initialRoute: '/',
        routes: {
          '/': (context) => const HomeScreen(),
          '/details': (context) => const ProductDetailsScreen(),
          '/cart': (context) => const CartScreen(),
          '/new-product': (context) => const NewProductScreen()
        },
      ),
    );
  }
}
