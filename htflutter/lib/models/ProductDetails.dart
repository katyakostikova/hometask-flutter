import 'package:htflutter/models/Picture.dart';
import 'package:htflutter/models/Seller.dart';

class ProductDetails {
  late int id;
  late String title;
  late String price;
  late String description;
  late List<Picture>? pictures;
  late Seller seller;

   ProductDetails.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        title = json['title'],
        price = json['price'].toString(),
        pictures = List.from(json['pictures'].map((json) => Picture.fromJson(json))),
        description = json['description'],
        seller = Seller.fromJson(json['seller']);
}