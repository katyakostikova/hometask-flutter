class CurrentUser {
  late int id;
  late String name;
  late String email;
  late String phoneNumber;
  late String? avatar;

   CurrentUser.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        name = json['name'],
        email = json['email'],
        phoneNumber = json['phoneNumber'],
        avatar = json['avatar'];
}