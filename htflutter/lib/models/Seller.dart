class Seller {
  late String name;
  late String email;
  late String phoneNumber;
  late String? avatar;

   Seller.fromJson(Map<String, dynamic> json)
      : name = json['name'],
        email = json['email'],
        phoneNumber = json['phoneNumber'],
        avatar = json['avatar'];
}