class CartItem {
  late String title;
  late String picture;
  late String price;

  CartItem({required this.title, required this.picture, required this.price});
}
