class Product {
  late int id;
  late String title;
  late String price;
  late String description;
  late String? picture;

  Product(
      {required this.id,
      required this.title,
      required this.description,
      required this.price,
      this.picture});

  Product.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        title = json['title'],
        price = json['price'].toString(),
        picture = json['picture'],
        description = json['description'];
}
