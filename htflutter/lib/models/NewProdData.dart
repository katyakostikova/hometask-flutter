class NewProdData {
  late String title;
  late String price;
  late String description;

  NewProdData(
      {required this.title, required this.price, required this.description});
}
