class Picture {
  late int id;
  late String url;

   Picture.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        url = json['url'];
}