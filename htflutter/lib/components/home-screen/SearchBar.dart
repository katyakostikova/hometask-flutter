import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:htflutter/blocs/products/products_bloc.dart';

class SearchBar extends StatefulWidget {
  const SearchBar({Key? key}) : super(key: key);

  @override
  State<SearchBar> createState() => _SearchBarState();
}

class _SearchBarState extends State<SearchBar> {
  final TextEditingController _controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          border: Border.all(color: Theme.of(context).dividerColor),
          color: Theme.of(context).cardColor),
      padding: const EdgeInsets.symmetric(horizontal: 3.0),
      margin: const EdgeInsets.only(bottom: 8.0),
      child:
          Row(crossAxisAlignment: CrossAxisAlignment.center, children: <Widget>[
        Flexible(
          child: TextField(
            controller: _controller,
            decoration: InputDecoration(
                hintText: 'Search',
                hintStyle: Theme.of(context).textTheme.bodyText1,
                focusColor: Theme.of(context).primaryColor),
            style: Theme.of(context).textTheme.bodyText1,
          ),
        ),
        GestureDetector(
          onTap: () {
            final filters = {
              'page': 1,
              'perPage': 100,
              'filter': _controller.text
            };
            context.read<ProductsBloc>().add(ProductsSearchEvent(filters));
            _controller.value = TextEditingValue(
              text: '',
              selection: TextSelection.fromPosition(
                const TextPosition(offset: 0),
              ),
            );
          },
          child: Icon(
            Icons.search,
            color: Theme.of(context).iconTheme.color,
            size: 30.0,
            semanticLabel: "Search bar button",
          ),
        )
      ]),
    );
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }
}
