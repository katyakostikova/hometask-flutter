import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:htflutter/blocs/products/products_bloc.dart';
import 'package:htflutter/components/ErrorMessage.dart';
import 'package:htflutter/components/Preloader.dart';
import 'package:htflutter/components/home-screen/ProductPreview.dart';
import 'package:htflutter/models/Product.dart';

class ProductsList extends StatefulWidget {
  final List<Product> products;
  const ProductsList({Key? key, this.products = const <Product>[]}) : super(key: key);

  @override
  State<ProductsList> createState() => _ProductsListState();
}

class _ProductsListState extends State<ProductsList> {
  final filters = {'page': 1, 'perPage': 4, 'filter': ''};
  final ScrollController _scrollController = ScrollController();

  @override
  void initState() {
    super.initState();
    _scrollController.addListener(() {
      if(_scrollController.position.pixels >= _scrollController.position.maxScrollExtent - 50) {
        filters.update('page', (value) => int.parse(value.toString()) + 1);
        context.read<ProductsBloc>().add(ProductsFetchedEvent(filters));
      }
    });
  }

@override
  void dispose() {
    super.dispose();
    _scrollController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<ProductsBloc, ProductsState>(
      listener: (context, state) {
        if (state is! ProductsErrorState) {
          return;
        }

        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Text(state.error),
        ));
      },
      child: BlocBuilder<ProductsBloc, ProductsState>(
        builder: (context, state) {
          if (state is ProductsInitialState) {
            context.read<ProductsBloc>().add(ProductsFetchedEvent(filters));
          }

          if (state is ProductsErrorState) {
            return const ErrorMessage();
          }

          if (state is ProductsFetchedState) {
            return RefreshIndicator(
              onRefresh: () async => context
                  .read<ProductsBloc>()
                  .add(ProductsFetchedEvent(filters)),
              child: ListView.builder(
                  controller: _scrollController,
                  shrinkWrap: true,
                  itemCount: state.products.length,
                  itemBuilder: (context, int index) {
                    return ProductPreview(product: state.products[index]);
                  }),
            );
          }
          return const Preloader();
        },
      ),
    );
  }
}
