import 'package:flutter/material.dart';
import 'package:htflutter/common/components/no-image.dart';
import 'package:htflutter/models/Product.dart';

class ProductDetailsScreenArguments {
  final int productId;

  ProductDetailsScreenArguments(
    this.productId,
  );
}

class ProductPreview extends StatelessWidget {
  final Product product;
  const ProductPreview({Key? key, required this.product}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => Navigator.pushNamed(context, '/details',
          arguments: ProductDetailsScreenArguments(product.id)),
      child: Container(
          margin: const EdgeInsets.all(10.0),
          decoration: BoxDecoration(
              color: Theme.of(context).cardColor,
              boxShadow: [
                BoxShadow(
                  color: Theme.of(context).shadowColor,
                  spreadRadius: 5,
                  blurRadius: 7,
                  offset: const Offset(0, 3),
                ),
              ],
              borderRadius: BorderRadius.circular(10)),
          width: 200.0,
          height: 150.0,
          child: Row(
            children: [
              SizedBox(
                  child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8.0),
                child: Image.network(
                  product.picture ?? NO_IMAGE,
                  width: 100,
                  height: 100,
                  fit: BoxFit.cover,
                ),
              )),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        SizedBox(
                          width: 170.0,
                          child: Text(
                            product.title,
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(fontSize: 18, color: Theme.of(context).textTheme.bodyText1?.color),
                          ),
                        ),
                        Text('\$${product.price}', style: TextStyle(color: Theme.of(context).textTheme.bodyText1?.color))
                      ],
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 3.0),
                      child: Text(
                        product.description,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(color: Theme.of(context).textTheme.bodyText1?.color)
                      ),
                    )
                  ],
                ),
              )
            ],
          )),
    );
  }
}
