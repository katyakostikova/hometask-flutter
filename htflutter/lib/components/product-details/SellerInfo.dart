import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:htflutter/blocs/cart/cart_bloc.dart';
import 'package:htflutter/common/components/no-image.dart';
import 'package:htflutter/models/CartItem.dart';
import 'package:htflutter/models/ProductDetails.dart';
import 'package:htflutter/models/Seller.dart';
import 'package:url_launcher/url_launcher.dart';

class SellerInfo extends StatelessWidget {
  final Seller seller;
  final ProductDetails product;
  const SellerInfo({Key? key, required this.seller, required this.product})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(color: Theme.of(context).cardColor),
      padding: const EdgeInsets.all(8.0),
      child: Column(
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              ClipRRect(
                borderRadius: BorderRadius.circular(40),
                child: Image.network(
                  seller.avatar ?? NO_IMAGE,
                  width: 80,
                  height: 80,
                  fit: BoxFit.cover,
                ),
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      seller.name,
                      style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                          color: Theme.of(context).textTheme.bodyText1?.color),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      seller.phoneNumber,
                      style: TextStyle(
                          color: Theme.of(context).textTheme.bodyText1?.color),
                    ),
                  )
                ],
              )
            ],
          ),
          Container(
            padding: const EdgeInsets.symmetric(vertical: 10.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                ElevatedButton(
                    onPressed: () => launch('tel://${seller.phoneNumber}'),
                    child: Row(
                      children: [
                        const Text('Call Seller'),
                        Transform(
                          alignment: Alignment.center,
                          transform: Matrix4.rotationY(3.14),
                          child: const Icon(
                            Icons.call_rounded,
                            size: 30,
                            color: Colors.white,
                          ),
                        )
                      ],
                    )),
                BlocListener<CartBloc, CartState>(
                  listener: (context, state) {
                    if (state is AddToCartEvent) {
                      return;
                    }

                    ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                        content: Text('Added to cart successfully'),
                        duration: Duration(seconds: 1)));
                  },
                  child: BlocBuilder<CartBloc, CartState>(
                    builder: (context, state) {
                      return Padding(
                        padding: const EdgeInsets.only(left: 50.0),
                        child: SizedBox(
                          width: 120,
                          child: ElevatedButton(
                              onPressed: () {
                                final cartItemPicture =
                                    product.pictures == null ||
                                            product.pictures!.length == 0
                                        ? NO_IMAGE
                                        : product.pictures![0].url;
                                CartItem cartItem = CartItem(
                                    title: product.title,
                                    price: product.price,
                                    picture: cartItemPicture);
                                context
                                    .read<CartBloc>()
                                    .add(AddToCartEvent(cartItem));
                              },
                              child: const Icon(
                                Icons.shopping_cart,
                                size: 30,
                                color: Colors.white,
                              )),
                        ),
                      );
                    },
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
