import 'package:flutter/material.dart';
import 'package:htflutter/models/ProductDetails.dart';

class ProductInfo extends StatelessWidget {
  final ProductDetails product;
  const ProductInfo({Key? key, required this.product}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                ConstrainedBox(
                  constraints: BoxConstraints(
                      maxWidth: MediaQuery.of(context).size.width / 1.3),
                  child: Text(
                    product.title,
                    style: TextStyle(
                        fontSize: 20.0,
                        color: Theme.of(context).textTheme.bodyText1?.color),
                    overflow: TextOverflow.ellipsis,
                  ),
                ),
                Text(
                  '\$${product.price}',
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 16,
                      color: Theme.of(context).textTheme.bodyText1?.color),
                )
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(10.0),
            child: Text(
              product.description,
              style: TextStyle(
                  fontSize: 16,
                  color: Theme.of(context).textTheme.bodyText1?.color),
            ),
          )
        ],
      ),
    );
  }
}
