// ignore_for_file: prefer_is_empty

import 'package:flutter/material.dart';
import 'package:htflutter/common/components/no-image.dart';
import 'package:htflutter/models/Picture.dart';

class PicturesCarousel extends StatefulWidget {
  late List<Picture>? pictures;
  PicturesCarousel({Key? key, required this.pictures}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _PicturesCarouselState();
}

class _PicturesCarouselState extends State<PicturesCarousel> {
  int currentPictureId = 0;

  void handlePrevImage() {
    setState(() {
      currentPictureId -= 1;
    });
  }

  void handleNextImage() {
    setState(() {
      currentPictureId += 1;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height / 3,
      padding: const EdgeInsets.all(8.0),
      child: Row(
        children: [
          GestureDetector(
            onTap: currentPictureId == 0 ? null : handlePrevImage,
            child: Container(
              margin: const EdgeInsets.only(right: 10.0),
              decoration: BoxDecoration(
                  border: Border.all(color: Theme.of(context).dividerColor), borderRadius: BorderRadius.circular(5)),
              child: currentPictureId == 0
                  ? const Icon(Icons.arrow_back, size: 35.0, color: Colors.grey)
                  : Icon(Icons.arrow_back,
                      size: 35.0, color: Theme.of(context).iconTheme.color),
            ),
          ),
          Expanded(
              child: widget.pictures == null || widget.pictures!.isEmpty
                  ? Image.network(NO_IMAGE)
                  : Image.network(widget.pictures![currentPictureId].url)),
          GestureDetector(
            onTap: currentPictureId + 1 == widget.pictures?.length ||
                    widget.pictures?.length == 0
                ? null
                : handleNextImage,
            child: Container(
              margin: const EdgeInsets.only(left: 10.0),
              decoration: BoxDecoration(
                  border: Border.all(color: Theme.of(context).dividerColor), borderRadius: BorderRadius.circular(5)),
              child: currentPictureId + 1 == widget.pictures?.length ||
                      widget.pictures?.length == 0
                  ? const Icon(Icons.arrow_forward,
                      size: 35.0, color: Colors.grey)
                  : Icon(Icons.arrow_forward,
                      size: 35.0, color: Theme.of(context).iconTheme.color),
            ),
          ),
        ],
      ),
    );
  }
}
