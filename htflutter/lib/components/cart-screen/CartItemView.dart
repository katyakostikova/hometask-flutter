import 'package:flutter/material.dart';
import 'package:htflutter/models/CartItem.dart';

class CartItemView extends StatelessWidget {
  final CartItem cartItem;
  const CartItemView({Key? key, required this.cartItem}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.all(10.0),
      padding: const EdgeInsets.all(10.0),
      decoration: BoxDecoration(
          boxShadow: [
            BoxShadow(
              color: Theme.of(context).shadowColor,
              spreadRadius: 3,
              blurRadius: 7,
              offset: const Offset(0, 3),
            )
          ],
          color: Theme.of(context).cardColor,
          borderRadius: BorderRadius.circular(10.0)),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Image.network(
            cartItem.picture,
            width: 80,
            height: 80,
            fit: BoxFit.cover,
          ),
          SizedBox(
              width: MediaQuery.of(context).size.width / 2,
              child: Text(
                cartItem.title,
                style: TextStyle(
                    fontSize: 16,
                    color: Theme.of(context).textTheme.bodyText1?.color),
                overflow: TextOverflow.ellipsis,
              )),
          Text(
            '\$${cartItem.price}',
            style: TextStyle(
                fontSize: 16,
                color: Theme.of(context).textTheme.bodyText1?.color),
          )
        ],
      ),
    );
  }
}
