import 'package:flutter/material.dart';

class Header extends StatelessWidget {
  const Header({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Theme.of(context).cardColor,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: GestureDetector(
              onTap: () => Navigator.pop(context),
              child: Icon(Icons.arrow_back,
                  size: 30.0,
                  color: Theme.of(context).iconTheme.color,
                  semanticLabel: "Back to products list"),
            ),
          ),
          Text(
            'Back',
            style: TextStyle(fontSize: 18.0, color: Theme.of(context).textTheme.bodyText1?.color),
          )
        ],
      ),
    );
  }
}
