import 'dart:io';

import 'package:htflutter/common/http/current-user.dart';
import 'package:htflutter/models/CurrentUser.dart';
import 'package:htflutter/models/NewProdData.dart';
import 'package:htflutter/models/Product.dart';
import 'package:htflutter/models/ProductDetails.dart';
import 'package:http/http.dart' as http;
import 'dart:convert' as convert;

class Service {
  static const String _authority = 'bsa-marketplace.azurewebsites.net';
  static const String _apiPath = '/api';

  static Future loadProducts(Map<String, Object> filters) async {
    List collection;
    late List<Product> _products;
    var response = await http.get(
        Uri.https(_authority, '$_apiPath/Products', {
          'page': '${filters["page"]}',
          'perPage': '${filters["perPage"]}',
          'filter': '${filters["filter"]}'
        }),
        headers: {
          HttpHeaders.authorizationHeader: TOKEN,
          HttpHeaders.contentTypeHeader: 'application/json'
        });
    if (response.statusCode == 200) {
      collection = convert.jsonDecode(response.body);
      _products = collection.map((json) => Product.fromJson(json)).toList();
    }

    return _products;
  }

  static Future loadProductById(int id) async {
    late ProductDetails _product;
    var response = await http
        .get(Uri.parse('https://$_authority$_apiPath/Products/$id'), headers: {
      HttpHeaders.authorizationHeader: TOKEN,
      HttpHeaders.contentTypeHeader: 'application/json'
    });
    if (response.statusCode == 200) {
      _product = ProductDetails.fromJson(convert.jsonDecode(response.body));
    }

    return _product;
  }

  static Future<CurrentUser> getCurrentUser() async {
    late CurrentUser _currentUser;
    var response = await http
        .get(Uri.parse('https://$_authority$_apiPath/Users/details'), headers: {
      HttpHeaders.authorizationHeader: TOKEN,
      HttpHeaders.contentTypeHeader: 'application/json'
    });
    if (response.statusCode == 200) {
      _currentUser = CurrentUser.fromJson(convert.jsonDecode(response.body));
    }

    return _currentUser;
  }

  static Future<ProductDetails> addProduct(NewProdData prod) async {
    late ProductDetails _product;

    var response = await http.post(
        Uri.parse('https://$_authority$_apiPath/Products'),
        headers: {
          HttpHeaders.authorizationHeader: TOKEN,
          HttpHeaders.contentTypeHeader: 'application/json'
        },
        body: convert.jsonEncode({
          "title": prod.title,
          "price": prod.price,
          "description": prod.description
        }).toString());
    if (response.statusCode == 200) {
      _product = ProductDetails.fromJson(convert.jsonDecode(response.body));
    }

    return _product;
  }

  static Future deleteProduct(int id) async {
    var response = await http.delete(
        Uri.parse('https://$_authority$_apiPath/Products/$id'),
        headers: {
          HttpHeaders.authorizationHeader: TOKEN,
          HttpHeaders.contentTypeHeader: 'application/json'
        },);
    if (response.statusCode == 200) {
      return id;
    }

    return id;
  }
}
