import 'package:flutter/material.dart';

final lightTheme = ThemeData(
    backgroundColor: const Color.fromRGBO(204, 219, 240, 1),
    cardColor: Colors.white,
    textTheme: const TextTheme(bodyText1: TextStyle(color: Colors.black)),
    shadowColor: Colors.grey.withOpacity(0.5),
    dividerColor: Colors.black,
    iconTheme: const IconThemeData(color: Colors.black));

final darkTheme = ThemeData(
    backgroundColor: const Color.fromRGBO(19, 24, 31, 1),
    cardColor: const Color.fromRGBO(55, 66, 82, 1),
    textTheme: const TextTheme(bodyText1: TextStyle(color: Colors.white)),
    shadowColor: Colors.black.withOpacity(0.5),
    dividerColor: Colors.white,
    iconTheme: const IconThemeData(color: Colors.white));
