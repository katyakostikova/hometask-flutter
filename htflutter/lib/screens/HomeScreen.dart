import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:htflutter/blocs/products/products_bloc.dart';
import 'package:htflutter/components/home-screen/ProductsList.dart';
import 'package:htflutter/components/home-screen/SearchBar.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 5.0),
          child: BlocProvider<ProductsBloc>(
            create: (context) => ProductsBloc(),
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.only(bottom: 5.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      SizedBox(
                        width: 120,
                        child: ElevatedButton(
                            onPressed: () =>
                                Navigator.pushNamed(context, '/cart'),
                            child: Row(
                              children: const [
                                Text('My cart'),
                                Icon(
                                  Icons.shopping_cart,
                                  size: 30,
                                  color: Colors.white,
                                ),
                              ],
                            )),
                      ),
                      SizedBox(
                        child: ElevatedButton(
                            onPressed: () =>
                                Navigator.pushNamed(context, '/new-product'),
                            child: const Text('Add new product')),
                      ),
                    ],
                  ),
                ),
                const SearchBar(),
                const Flexible(child: ProductsList()),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
