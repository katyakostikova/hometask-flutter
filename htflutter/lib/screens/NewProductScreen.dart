import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:htflutter/blocs/products/products_bloc.dart';
import 'package:htflutter/components/Header.dart';
import 'package:htflutter/models/NewProdData.dart';

class NewProductScreen extends StatefulWidget {
  const NewProductScreen({Key? key}) : super(key: key);

  @override
  State<NewProductScreen> createState() => _NewProductScreenState();
}

class _NewProductScreenState extends State<NewProductScreen> {
  final _formKey = GlobalKey<FormState>();

  String? _title;
  String? _description;
  String? _price;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Theme.of(context).backgroundColor,
      body: SafeArea(
        child: Column(
          children: [
            const Header(),
            Container(
              margin: const EdgeInsets.only(top: 50.0),
              decoration:
                  BoxDecoration(color: Theme.of(context).cardColor, boxShadow: [
                BoxShadow(
                  color: Theme.of(context).shadowColor,
                  spreadRadius: 3,
                  blurRadius: 7,
                  offset: const Offset(0, 3),
                )
              ]),
              padding: const EdgeInsets.all(8.0),
              width: MediaQuery.of(context).size.width / 1.5,
              height: MediaQuery.of(context).size.height / 1.5,
              child: Form(
                key: _formKey,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    TextFormField(
                      onSaved: (value) {
                        _title = value;
                      },
                      decoration: InputDecoration(
                          hintText: 'Enter product title',
                          hintStyle: TextStyle(
                              color: Theme.of(context)
                                  .textTheme
                                  .bodyText1
                                  ?.color)),
                      style: Theme.of(context).textTheme.bodyText1,
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'Please enter some text';
                        }
                        return null;
                      },
                    ),
                    TextFormField(
                      decoration: InputDecoration(
                        hintText: 'Enter product price',
                        hintStyle: TextStyle(
                            color:
                                Theme.of(context).textTheme.bodyText1?.color),
                      ),
                      keyboardType: TextInputType.number,
                      style: Theme.of(context).textTheme.bodyText1,
                      onSaved: (value) {
                        _price = value;
                      },
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'Please enter a number > 0';
                        }
                        return null;
                      },
                    ),
                    TextFormField(
                      decoration: InputDecoration(
                        hintText: 'Enter product description',
                        hintStyle: TextStyle(
                            color:
                                Theme.of(context).textTheme.bodyText1?.color),
                      ),
                      style: Theme.of(context).textTheme.bodyText1,
                      onSaved: (value) {
                        _description = value;
                      },
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'Please enter some text';
                        }
                        return null;
                      },
                    ),
                    ElevatedButton(
                        onPressed: () {
                          if (_formKey.currentState!.validate()) {
                            _formKey.currentState!.save();

                            context.read<ProductsBloc>().add(AddProductEvent(
                                NewProdData(
                                    title: _title as String,
                                    price: _price as String,
                                    description: _description as String)));
                            Navigator.pushReplacementNamed(context, '/');
                          }
                        },
                        child: const Text('Add new product'))
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
