import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:htflutter/blocs/products/products_bloc.dart';
import 'package:htflutter/components/Preloader.dart';
import 'package:htflutter/components/home-screen/ProductPreview.dart';
import 'package:htflutter/components/Header.dart';
import 'package:htflutter/components/product-details/PicturesCarousel.dart';
import 'package:htflutter/components/product-details/ProductInfo.dart';
import 'package:htflutter/components/product-details/SellerInfo.dart';
import 'package:htflutter/models/CurrentUser.dart';
import 'package:htflutter/services/Service.dart';

class ProductDetailsScreen extends StatelessWidget {
  const ProductDetailsScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final params = ModalRoute.of(context)!.settings.arguments
        as ProductDetailsScreenArguments;
    return FutureBuilder<CurrentUser>(
        future: Service.getCurrentUser(),
        builder: (context, AsyncSnapshot<CurrentUser> currentUser) {
          if (currentUser.hasData) {
            return Scaffold(
                backgroundColor: Theme.of(context).backgroundColor,
                body: SafeArea(
                  child: BlocProvider(
                    create: (context) => ProductsBloc(),
                    child: BlocListener<ProductsBloc, ProductsState>(
                      listener: (context, state) {
                        if (state is! ProductsErrorState) {
                          return;
                        }
                        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                          content: Text(state.error),
                        ));
                      },
                      child: BlocBuilder<ProductsBloc, ProductsState>(
                          builder: (context, state) {
                        if (state is ProductsInitialState) {
                          context
                              .read<ProductsBloc>()
                              .add(ProductFetchedEvent(params.productId));
                        }

                        if (state is ProductsErrorState) {
                          return const Text('Error');
                        }

                        if (state is ProductFetchedState) {
                          return Column(
                            children: [
                              const Header(),
                              Expanded(
                                child: SingleChildScrollView(
                                  child: Column(
                                    children: [
                                      PicturesCarousel(
                                        pictures: state.product.pictures,
                                      ),
                                      if (currentUser.data?.phoneNumber ==
                                          state.product.seller.phoneNumber)
                                        ElevatedButton(
                                          onPressed: () {
                                            context.read<ProductsBloc>().add(
                                                DeleteProductEvent(
                                                    state.product.id));
                                            Navigator.pushReplacementNamed(
                                                context, '/');
                                          },
                                          child: const Text('Delete product'),
                                          style: ElevatedButton.styleFrom(
                                              primary: Colors.red),
                                        ),
                                      ProductInfo(
                                        product: state.product,
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              SellerInfo(
                                  seller: state.product.seller,
                                  product: state.product)
                            ],
                          );
                        }
                        return const Preloader();
                      }),
                    ),
                  ),
                ));
          } else {
            return const Preloader();
          }
        });
  }
}
