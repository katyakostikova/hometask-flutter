import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:htflutter/blocs/cart/cart_bloc.dart';
import 'package:htflutter/components/Header.dart';
import 'package:htflutter/components/cart-screen/CartItemView.dart';

class CartScreen extends StatelessWidget {
  const CartScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      body: SafeArea(
        child: BlocBuilder<CartBloc, CartState>(builder: (context, state) {
          if (state is CartAddedState) {
            return Column(
              children: [
                const Header(),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        margin: const EdgeInsets.only(right: 30.0, left: 80.0),
                        child: Text(
                          'Your Cart',
                          style: TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.bold,
                              color:
                                  Theme.of(context).textTheme.bodyText1?.color),
                        ),
                      ),
                      ElevatedButton(
                        style: ElevatedButton.styleFrom(primary: Colors.red),
                        onPressed: () =>
                            {context.read<CartBloc>().add(ClearCartEvent())},
                        child: const Icon(
                          Icons.delete,
                          color: Colors.white,
                          size: 30.0,
                        ),
                      )
                    ],
                  ),
                ),
                Flexible(
                  child: ListView.builder(
                      itemCount: state.cart.length,
                      itemBuilder: (context, int index) {
                        return CartItemView(cartItem: state.cart[index]);
                      }),
                )
              ],
            );
          }
          return Column(
            children: [
              const Header(),
              Padding(
                padding: const EdgeInsets.all(10.0),
                child: Center(
                  child: Text(
                    'Your cart is empty',
                    style: TextStyle(
                        fontSize: 20,
                        color: Theme.of(context).textTheme.bodyText1?.color),
                  ),
                ),
              ),
            ],
          );
        }),
      ),
    );
  }
}
