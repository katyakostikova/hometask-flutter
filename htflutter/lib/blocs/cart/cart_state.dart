part of 'cart_bloc.dart';

abstract class CartState {
  List<CartItem> cart = [];
}

class CartInitial extends CartState {
  CartInitial();
}

class CartAddedState extends CartState {
  List<CartItem> cart;
  CartAddedState(this.cart);
}


class CartClearedState extends CartState {
  List<CartItem> cart;
  CartClearedState(this.cart);
}
