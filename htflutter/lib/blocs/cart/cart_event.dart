part of 'cart_bloc.dart';

@immutable
abstract class CartEvent {}

class AddToCartEvent extends CartEvent {
  final CartItem cartItem;

  AddToCartEvent(this.cartItem);
}

class ClearCartEvent extends CartEvent {
  ClearCartEvent();
}
