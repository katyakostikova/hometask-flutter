import 'package:bloc/bloc.dart';
import 'package:htflutter/models/CartItem.dart';
import 'package:meta/meta.dart';

part 'cart_event.dart';
part 'cart_state.dart';

class CartBloc extends Bloc<CartEvent, CartState> {
  CartBloc() : super(CartInitial()) {
    on<AddToCartEvent>(handleCartEvent);
    on<ClearCartEvent>(handleCartEvent);
  }

   Future<void> handleCartEvent(
      CartEvent event, Emitter<CartState> emit) async{
    if (event is AddToCartEvent) {
      final newCart = [...state.cart, event.cartItem];
      emit(CartAddedState(newCart));
    }else if (event is ClearCartEvent) {
      List<CartItem> clearedCart = [];
      emit(CartAddedState(clearedCart));
    }
  }
}
