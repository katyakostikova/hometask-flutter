part of 'products_bloc.dart';

@immutable
abstract class ProductsState {
  late List<Product> products = [];
  ProductsState();
}

class ProductsInitialState extends ProductsState {
  ProductsInitialState();
}

class ProductsLoadingState extends ProductsState {
  ProductsLoadingState();
}

class ProductsFetchedState extends ProductsState {
  final List<Product> products;

  ProductsFetchedState(this.products);
}

class ProductFetchedState extends ProductsState {
  final ProductDetails product;

  ProductFetchedState(this.product);
}

class ProductsErrorState extends ProductsState {
  final String error;

  ProductsErrorState(this.error);
}
