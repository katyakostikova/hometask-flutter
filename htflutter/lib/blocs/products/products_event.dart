part of 'products_bloc.dart';

@immutable
abstract class ProductsEvent {
  const ProductsEvent();
}

class ProductsFetchedEvent extends ProductsEvent {
  final Map<String, Object> filters;
  const ProductsFetchedEvent(this.filters);
}

class ProductsSearchEvent extends ProductsEvent {
  final Map<String, Object> filters;
  const ProductsSearchEvent(this.filters);
}

class ProductFetchedEvent extends ProductsEvent {
  final int id;

  const ProductFetchedEvent(this.id);
}

class AddProductEvent extends ProductsEvent {
  final NewProdData prodData;

  const AddProductEvent(this.prodData);
}

class DeleteProductEvent extends ProductsEvent {
  final int id;

  const DeleteProductEvent(this.id);
}