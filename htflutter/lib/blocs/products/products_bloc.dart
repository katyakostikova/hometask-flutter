import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:htflutter/models/NewProdData.dart';
import 'package:htflutter/models/Product.dart';
import 'package:htflutter/models/ProductDetails.dart';
import 'package:htflutter/services/Service.dart';

part 'products_event.dart';
part 'products_state.dart';

class ProductsBloc extends Bloc<ProductsEvent, ProductsState> {
  ProductsBloc() : super(ProductsInitialState()) {
    on<ProductsFetchedEvent>(handleProductFetchEvent);
    on<ProductFetchedEvent>(handleProductFetchEvent);
    on<AddProductEvent>(handleProductFetchEvent);
    on<DeleteProductEvent>(handleProductFetchEvent);
    on<ProductsSearchEvent>(handleProductFetchEvent);
  }

  Future<void> handleProductFetchEvent(
      ProductsEvent event, Emitter<ProductsState> emit) async {
    if (event is ProductsInitialState) {
      emit(ProductsLoadingState());
    } else if (event is ProductsFetchedEvent) {
      try {
        final products = await Service.loadProducts(event.filters);
        List<Product> newFetched = [...state.products, ...products];
        emit(ProductsFetchedState(newFetched));
      } catch (error) {
        emit(ProductsErrorState('An error occured'));
      }
    } else if (event is ProductsSearchEvent) {
      try {
        final products = await Service.loadProducts(event.filters);
        emit(ProductsFetchedState(products));
      } catch (error) {
        emit(ProductsErrorState('An error occured'));
      }
    } else if (event is ProductFetchedEvent) {
      emit(ProductsLoadingState());
      try {
        final product = await Service.loadProductById(event.id);
        emit(ProductFetchedState(product));
      } catch (error) {
        emit(ProductsErrorState('An error occured'));
      }
    } else if (event is AddProductEvent) {
      emit(ProductsLoadingState());
      try {
        final product = await Service.addProduct(event.prodData);
        final newProducts = [
          ...state.products,
          Product(
              id: product.id,
              title: product.title,
              description: product.description,
              price: product.price,
              picture: product.pictures?.first.url)
        ];
        emit(ProductsFetchedState(newProducts));
      } catch (error) {
        emit(ProductsErrorState('An error occured'));
      }
    } else if (event is DeleteProductEvent) {
      emit(ProductsLoadingState());
      try {
        final deletedProductId = await Service.deleteProduct(event.id);
        final listToUpdate = [...state.products];
        listToUpdate.removeWhere((element) => element.id == deletedProductId);
        emit(ProductsFetchedState(listToUpdate));
      } catch (error) {
        emit(ProductsErrorState('An error occured'));
      }
    }
  }
}
